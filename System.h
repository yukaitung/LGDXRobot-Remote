#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <QTcpServer>

#include <memory>

#include "SystemData.h"

class System : public QObject
{
    Q_OBJECT

private:
    static System *instance;
    explicit System(QObject *parent = nullptr);

    // Var
    const int kMyPort = 34689;
    const int kRemotePort = 34777;
    SystemData *data;

    // TCP
    std::unique_ptr<QTcpServer> tcpInServer;
    QTcpSocket *tcpInClient; // Auto delete
    std::unique_ptr<QTcpSocket> tcpOutClient;

public:    
    static System *getInstance();

public slots:
    // Communication
    void remoteHello();
    void remoteAutoCollection();
    void remoteAutoExplore();
    void remoteManual();
    void remoteManualVelocity(float x, float y, float w);
    void remoteManualServo(int node, int angle);
    void remoteIdle();
    void tcpConnect(QString ip);
    void tcpSend(QString str);
    void tcpInNewConnection();
    void tcpInReceive();

signals:

};

#endif // SYSTEM_H
