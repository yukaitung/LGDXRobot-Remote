import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import QtQuick.VirtualKeyboard 2.15
import QtWebView 1.15
import RobotStateClass 1.0
import TCPConnectStateClass 1.0

Window {
    id: window
    width: 1920
    height: 1080
    visible: true
    title: qsTr("LGDX Robot Remote Control")

    property double strightSpeed: 0.1
    property double rotateSpeed: 1.1

    /*
     * Background
     */
    Rectangle {
        anchors.fill: parent
        color: 'black'
    }

    /*
     * Header
     */
    Item {
        id: header
        width: parent.width - 32
        anchors.horizontalCenter: parent.horizontalCenter
        height: 32
        anchors.top: parent.top

        Text {
            anchors.verticalCenter: parent.verticalCenter
            text: window.title
            color: 'white'
        }

        Text {
            id: clock
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            color: 'white'
            Timer {
                running: true
                repeat: true
                interval: 1000
                onTriggered: {
                    var date = new Date();
                    clock.text = date.toTimeString();
                }
            }

        }
    }

    /*
     * Foreground
     */
    Rectangle {
        id: foreground
        width: parent.width
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        color: 'white'
        radius: 5

        Rectangle {
            width: foreground.width
            height: foreground.radius
            color: foreground.color
            anchors.left: parent.left
            anchors.bottom: parent.bottom
        }
    }

    /*
     * Main
     */
    Grid {
        id : mainGrid
        width: parent.width - 32
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.bottom
        anchors.topMargin: 16
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 16
        columns : 2
        spacing: 16

        Pane {
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            ColumnLayout {
                width: parent.width
                height: parent.height
                Text {
                    text: qsTr("Robot Control")
                }
                Row{
                    spacing: 8
                    Button {
                        text: qsTr("Connect To a Robot")
                        onClicked: connectDialog.open()
                    }
                    Text {
                        text: qsTr("Disconnected")
                        visible: systemData.tcpConnectState === TCPConnectState.Disconnected
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        text: qsTr("Connecting To %1").arg(systemData.robotIP)
                        visible: systemData.tcpConnectState === TCPConnectState.Connecting
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        text: qsTr("Connected To %1").arg(systemData.robotIP)
                        visible: systemData.tcpConnectState === TCPConnectState.Connected
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        text: qsTr("Error: %1").arg(systemData.tcpConnectError)
                        visible: systemData.tcpConnectState === TCPConnectState.Error
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
                Text {
                    text: qsTr("Robot Mode")
                }
                Row {
                    spacing: 8
                    Button {
                        id: autoExploreButton
                        text: qsTr("Auto Explore")
                        onClicked: system.remoteAutoExplore()
                        enabled: (systemData.tcpConnectState === TCPConnectState.Connected) && systemData.robotState === RobotState.Idle
                    }
                    Button {
                        id: autoCollectionButton
                        text: qsTr("Auto Collection")
                        onClicked: system.remoteAutoCollection()
                        enabled: (systemData.tcpConnectState === TCPConnectState.Connected) && systemData.robotState === RobotState.Idle
                    }
                    Button {
                        id: manualModeButton
                        text: qsTr("Manual Mode")
                        onClicked: system.remoteManual()
                        enabled: (systemData.tcpConnectState === TCPConnectState.Connected) && systemData.robotState === RobotState.Idle
                    }
                    Button {
                        id: idleButton
                        text: qsTr("Stop Robot")
                        onClicked: system.remoteIdle()
                        enabled: (systemData.tcpConnectState === TCPConnectState.Connected) && systemData.robotState !== RobotState.Idle && systemData.robotState !== RobotState.NotInit && systemData.robotState !== RobotState.Stopping
                    }
                }
                Text {
                    text: qsTr("Sensors Information")
                }
                Row {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    HomeSensorInfo {
                        width: parent.width / 4
                        imageLocation: "img/wheel.png"
                        sensorName: qsTr("Wheel 1")
                        sensorValue: qsTr("%1 rad").arg(systemData.wheelSpeed[0])
                    }
                    HomeSensorInfo {
                        width: parent.width / 4
                        imageLocation: "img/wheel.png"
                        sensorName: qsTr("Wheel 2")
                        sensorValue: qsTr("%1 rad").arg(systemData.wheelSpeed[1])
                    }
                    HomeSensorInfo {
                        width: parent.width / 4
                        imageLocation: "img/wheel.png"
                        sensorName: qsTr("Wheel 3")
                        sensorValue: qsTr("%1 rad").arg(systemData.wheelSpeed[2])
                    }
                    HomeSensorInfo {
                        width: parent.width / 4
                        imageLocation: "img/wheel.png"
                        sensorName: qsTr("Wheel 4")
                        sensorValue: qsTr("%1 rad").arg(systemData.wheelSpeed[3])
                    }
                }
                Row {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/laser.png"
                        sensorName: qsTr("Laser 1")
                        sensorValue: qsTr("%1 mm").arg(systemData.laser[0])
                    }
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/laser.png"
                        sensorName: qsTr("Laser 2")
                        sensorValue: qsTr("%1 mm").arg(systemData.laser[1])
                    }
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/laser.png"
                        sensorName: qsTr("Laser 3")
                        sensorValue: qsTr("%1 mm").arg(systemData.laser[2])
                    }
                }
                Row {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/camera.png"
                        sensorName: qsTr("D435")
                        sensorValue: systemData.d400 ? qsTr("Available") : qsTr("Unavailable")
                    }
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/camera.png"
                        sensorName: qsTr("T265")
                        sensorValue: systemData.t265 ? qsTr("Available") : qsTr("Unavailable")
                    }
                    HomeSensorInfo {
                        width: parent.width / 3
                        imageLocation: "img/mcu.png"
                        sensorName: qsTr("MCU")
                        sensorValue: systemData.mcu ? qsTr("Available") : qsTr("Unavailable")
                    }
                }
            }
        }

        /*
         * Welcome for unconnected
         */
        Pane {
            id: welcomeControlPanel
            visible: (systemData.tcpConnectState !== TCPConnectState.Connected)
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2

            Column {
                width: parent.width
                Text {
                    text: qsTr("Welcome")
                }
                Text {
                    text: qsTr("Press \"Connect to Robot\" and input the IP address to begin.")
                }
            }
        }

        /*
         * Welcome for connected
         */
        Pane {
            id: idleControlPanel
            visible: (systemData.tcpConnectState === TCPConnectState.Connected) && (systemData.robotState === RobotState.NotInit || systemData.robotState === RobotState.Idle || systemData.robotState === RobotState.Stopping)
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            Column {
                width: parent.width
                Text {
                    text: qsTr("Welcome")
                }
                Text {
                    text: qsTr("You can change the \"Robot Mode\" on the left side to command the robot.\n")
                    + qsTr("\"Auto explore\" let the robot explore and generating map automatically.\n")
                    + qsTr("\"Auto collection\" let the robot to collect rubbish automatically with given map.\n")
                    + qsTr("\"Manual mode\" allows you to control the robot.\n")
                    + qsTr("\"Stop robot\" to stop any operation.\n\n")
                    + qsTr("\"Sensor information\" reports real-time data from wheel counter and laser, it also show whether the hardware is connected or not.\n")
                    + qsTr("\"Camera\" displays the steaming form the robot.\n")
                    + qsTr("\"Log\" shows the log form the robot, swipe to change the log form different nodes. Press \"Clear all log\" to clear log.\n")
                }
            }
        }

        /*
         * Auto mode
         */
        Pane {
            id: autoControlPanel
            visible: (systemData.tcpConnectState === TCPConnectState.Connected) && (systemData.robotState === RobotState.AutoCollection || systemData.robotState === RobotState.AutoExplore)
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            onVisibleChanged: ros2dView.reload()
            ColumnLayout {
                width: parent.width
                height: parent.height
                Text {
                    text: qsTr("Auto Mode")
                }
                WebView {
                    id: ros2dView

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    url: systemData.ros2dPath
                    onLoadingChanged: {
                        if(ros2dView.loadProgress === 100)
                        {
                            if(systemData.robotIP === "")
                                return;
                            ros2dView.runJavaScript("init(\"" + systemData.robotIP + "\")");
                        }
                    }
                }
                Button {
                    enabled: (systemData.tcpConnectState === TCPConnectState.Connected)
                    text: qsTr("Refresh")
                    onClicked: ros2dView.reload()
                    Layout.alignment: Qt.AlignRight
                }
            }
        }

        /*
         * Manual mode
         */
        Pane {
            id: manualControlPanel
            visible: (systemData.tcpConnectState === TCPConnectState.Connected) && (systemData.robotState === RobotState.ManualAll)
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            ColumnLayout {
                width: parent.width
                height: parent.height
                Text {
                    text: qsTr("Manual Mode")
                }
                RowLayout {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillHeight: true
                    spacing: 32
                    Button {
                        text: "↶"
                        onPressed: system.remoteManualVelocity(0, 0, rotateSpeed)
                        onReleased: system.remoteManualVelocity(0, 0, 0)
                    }
                    Grid {
                        columns: 3
                        rows: 3
                        spacing: 32
                        Button {
                            text: "🢄"
                            onPressed: system.remoteManualVelocity(strightSpeed, strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢁"
                            onPressed: system.remoteManualVelocity(strightSpeed, 0, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢅"
                            onPressed: system.remoteManualVelocity(strightSpeed, -strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢀"
                            onPressed: system.remoteManualVelocity(0, strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Rectangle {
                            width: 1
                            height: 1
                        }
                        Button {
                            text: "🢂"
                            onPressed: system.remoteManualVelocity(0, -strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢇"
                            onPressed: system.remoteManualVelocity(-strightSpeed, strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢃"
                            onPressed: system.remoteManualVelocity(-strightSpeed, 0, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                        Button {
                            text: "🢆"
                            onPressed: system.remoteManualVelocity(-strightSpeed, -strightSpeed, 0)
                            onReleased: system.remoteManualVelocity(0, 0, 0)
                        }
                    }
                    Button {
                        Layout.alignment: Qt.AlignRight
                        text: "↷"
                        onPressed: system.remoteManualVelocity(0, 0, -rotateSpeed)
                        onReleased: system.remoteManualVelocity(0, 0, 0)
                    }
                }
                GridLayout {
                    Layout.preferredWidth: parent.width
                    columns: 5
                    rows: 3

                    // Label
                    Text {
                        text: qsTr("Door")
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Text {
                        text: qsTr("Shovel")
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Text {
                        text: qsTr("Bin 1")
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Text {
                        text: qsTr("Bin 2")
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Text {
                        text: qsTr("Bin 3")
                        Layout.alignment: Qt.AlignHCenter
                    }

                    // Up
                    Button {
                        text: qsTr("UP")
                        onClicked: system.remoteManualServo(15, 90)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("UP")
                        onClicked: system.remoteManualServo(4, 140)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("UP")
                        onClicked: system.remoteManualServo(5, 90)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("UP")
                        onClicked: system.remoteManualServo(6, 90)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("UP")
                        onClicked: system.remoteManualServo(7, 90)
                        Layout.alignment: Qt.AlignHCenter
                    }

                    // Down
                    Button {
                        text: qsTr("Down")
                        onClicked: system.remoteManualServo(15, 0)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("Down")
                        onClicked: system.remoteManualServo(4, 0)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("Down")
                        onClicked: system.remoteManualServo(5, 0)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("Down")
                        onClicked: system.remoteManualServo(6, 0)
                        Layout.alignment: Qt.AlignHCenter
                    }
                    Button {
                        text: qsTr("Down")
                        onClicked: system.remoteManualServo(7, 0)
                        Layout.alignment: Qt.AlignHCenter
                    }
                }
            }
        }

        Pane {
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            ColumnLayout {
                width: parent.width
                height: parent.height
                Text {
                    text: qsTr("Camera")
                }
                Rectangle {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    color: 'black'
                    Text {
                        id: cameraHint
                        text: qsTr("Connect to a robot to see the streaming.")
                        visible: (systemData.tcpConnectState !== TCPConnectState.Connected)
                        anchors.centerIn: parent
                        color: 'white'
                    }
                    WebView {
                        id: cameraWebView
                        visible: (systemData.tcpConnectState === TCPConnectState.Connected)
                        enabled: false
                        anchors.fill: parent
                        url: systemData.robotVideoUrl
                        onVisibleChanged: cameraWebView.reload()
                    }
                }
                Button {
                    enabled: (systemData.tcpConnectState === TCPConnectState.Connected)
                    text: qsTr("Refresh")
                    onClicked: cameraWebView.reload()
                }
            }
        }

        /*
         * Log
         */
        Pane {
            width: parent.width / 2 - mainGrid.spacing / 2
            height: parent.height / 2 - mainGrid.spacing / 2
            clip: true
            ColumnLayout {
                width: parent.width
                height: parent.height
                Text {
                    text: qsTr("Log")
                }
                Page {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    header: TabBar {
                        id: logTabBar
                        currentIndex: logSwipeView.currentIndex

                        TabButton {
                            text: "D400"
                        }
                        TabButton {
                            text: "T265"
                        }
                        TabButton {
                            text: "Rtabmap"
                        }
                        TabButton {
                            text: "Move Base"
                        }
                        TabButton {
                            text: "Explore"
                        }
                        TabButton {
                            text: "PathPlanner"
                        }
                    }
                    SwipeView {
                        id: logSwipeView
                        anchors.fill: parent
                        currentIndex: logTabBar.currentIndex
                        clip: true
                        // D400
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.d400Log
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                        // T265
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.t265Log
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                        // Rtabmap
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.rtabmapLog
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                        // Move base
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.moveBaseLog
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                        // Move base
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.exploreLog
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                        // Path Planner
                        ListView {
                            width: logSwipeView.width
                            height: logSwipeView.height
                            model: systemData.pathPlannerLog
                            delegate: Text {
                                text: modelData
                            }
                            clip: true
                            onCountChanged: {
                                this.currentIndex = count - 1
                            }
                            ScrollBar.vertical: ScrollBar {}
                        }
                    }
                }
                Button {
                    text: qsTr("Clear All Log")
                    Layout.alignment: Qt.AlignRight
                    onClicked: systemData.clearAllLog();
                }
            }
        }
    }

    /*
     * Footer
     */
    /*
    Item {
        id: footer
        width: parent.width - 32
        height: 48
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

    }
    */

    /*
     * Other Stuff
     */
    /*
    Connections {
        target: systemData
        function onRobotIPChanged() {
            cameraHint.visible = false;
            cameraWebView.visible = true;
            cameraWebView.reload();

        }
    }
    */
    Dialog {
        id: connectDialog
        visible: false
        title: qsTr("Connection")
        x: (window.width - width) / 2
        y: (window.height - height) / 2 - (window.height - inputPanel.y) / 2
        parent: ApplicationWindow.overlay
        width: parent.width * 0.4
        height: contentChildren.height
        focus: true
        modal: true
        closePolicy: Popup.NoAutoClose
        Column {
            width: parent.width

            Text {
                text: qsTr("Enter the IP address of the robot")
            }
            TextField {
                id: connectDialogIPaddr
                placeholderText: qsTr("IP address")
                inputMethodHints: Qt.ImhDigitsOnly
            }
        }
        onOpened: {
            connectDialogIPaddr.text = systemData.robotIP;
        }
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            system.tcpConnect(connectDialogIPaddr.text);
        }
        onClosed: {
            connectDialogIPaddr.text = "";
        }
    }


    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: window.height
        width: window.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: window.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
